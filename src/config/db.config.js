// change the info below to access to your localhost
module.exports = {
    HOST: "localhost",
    PORT: '3306',
    USER: "[YOUR_USERNAME]",
    PASSWORD: "[YOUR_PASSWORD]",
    DB: "purrfect-match_dev"
};
